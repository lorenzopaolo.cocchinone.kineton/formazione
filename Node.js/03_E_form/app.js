import express from 'express';
import cors from 'cors';
//import multer from 'multer'; multipart/form-data 
import { createConnection } from 'mysql';
import helmet from 'helmet';

const app = express();
const port = 3000;
//const upload = multer(); multipart/form-data 
const server = app.listen(port);

app.use(cors());
app.use(helmet()); 
app.use(express.json());


const connection = createConnection({
    host: "localhost",
    user: "root",
    password: "@EYmFkcwB4^S4*tfNNk&",
    database: "express_db",
  });
  
  connection.connect((err) => {
    if (err) {
      console.error("Error Connecting to MySql: " + err.stack);
      return;
    }
    console.log("connected as id " + connection.threadId);
  });

app.get('/users', async (req, res) => {
    connection.query(`select * from users where id < 10`, (err, rows, fields) => {
        console.log(`Richiesti i primi 10 utenti: `);
        console.log(rows);
        res.send(rows);
    });
});


app.get('/user/:id', async (req, res) => {
    connection.query(`select * from users where id = ${req.params.id}`, (err, rows, fields) => {
        console.log(`Richiesto utente # ${req.params.id} `);
        console.log(rows);
        res.send(rows);
    });
});

/*  multipart/form-data 
    app.post('/', upload.fields([]), (req, res) => {
    let data = Object.values(req.body);
    connection.query(`insert into users (firstname, lastname, email) values ('${data[0]}', '${data[1]}', '${data[2]}')`, (err, rows, fields) => {        
        if(!err){
            console.log(`Saved: `);
            console.log(req.body);
            console.log(`Database Response: `);
            console.log(rows);
            res.json(`Data Saved in Database, Affected Rows: ${rows.affectedRows}, Inserted Id: ${rows.insertId}`);
        } else {
            res.json(err);
        }
    });
}); */


app.post('/', (req, res) => {
    let data = {};
    console.log(typeof req.body);
    for(const [key, value] of Object.entries(req.body)){
        data[key] = value;
        
    }

    connection.query(`insert into users (firstname, lastname, email) values ('${data.firstname}', '${data.lastname}', '${data.email}')`, (err, rows, fields) => {        
        if(!err){
            console.log(`Saved: `);
            console.log(req.body);
            console.log(`Database Response: `);
            console.log(rows);
            res.json(`Data Saved in Database, Affected Rows: ${rows.affectedRows}, Inserted Id: ${rows.insertId}`);
        } else {
            res.json(err);
        }
    });
});

server.on('listening', () => {
    console.log(`Listening on port ${port}`);
});

process.on('SIGINT', () =>{
    console.log('SIGINT signal received: closing HTTP server...');
    server.close(()=>{
        console.log('HTTP Server Closed.');
    });  
    process.exit(0);
});