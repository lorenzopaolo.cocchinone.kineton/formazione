var j = 50;

printJ();

function greetMe(yourName){
    console.log(`Hello ${yourName}`);
}

function printJ(){
    console.log(j);
}

greetMe('World');
printJ()    ;

let x = 45;
console.log(x);

console.log(window.j);
console.log(globalThis.j);

const coffees = ['French Roast', 
    'Colombian', 
    'Kona',
];

const fish = ['Tuna', 
    'Hammer',
];

console.log(coffees);
console.log(fish)
console.log(coffees.length);

const a = new Boolean(true)
console.log(a.valueOf());

function map(f, a) {
    const result = new Array(a.length);
    for (let i = 0; i < a.length; i++){
        result[i] = f(a[i]);
    }

    return result;
}

const f = function (x) {
    return x * x * x;
}

const numbers = [0, 1, 2, 5, 10];
const cube = map(f, numbers);
console.log(cube);

