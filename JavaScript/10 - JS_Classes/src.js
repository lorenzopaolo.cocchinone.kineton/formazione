import { Animal } from "./Animal.js";


class Rabbit extends Animal {

    constructor(name, earLength){
        super(name);
        this.earLength = earLength;
    }


    hide(){
        console.log(`${this.name} hide!`)
    }

    stop(){
        super.stop();
        this.hide();
    }
}


let rabbit = new Rabbit("White Rabbit", "10cm");
console.log(rabbit);
rabbit.stop();
console.log(rabbit instanceof Object);