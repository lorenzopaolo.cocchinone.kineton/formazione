let firstOperand = "";
let secondOperand = "";
let operation = "";
let outputValue = "";
let outputArea = document.querySelector("#operand-1");

const operations = document.querySelectorAll(".operation");
operations.forEach((operation) => {
  operation.addEventListener("click", getOperationAndSetFirstOp);
});

const numbers = document.querySelectorAll(".number");
numbers.forEach((number) => {
  number.addEventListener("click", updateOutArea);
});

const equal = document.querySelector(".equal");
equal.addEventListener("click", getOutput);

const reset = document.querySelector(".reset");
reset.addEventListener("click", resetAll);

function updateOutArea() {
  if (outputArea.textContent.toLocaleUpperCase() == "0".toLocaleUpperCase()) {
    outputArea.textContent = "";
  }
  outputArea.textContent += this.textContent;
}

function getOperationAndSetFirstOp() {
  firstOperand = Number(outputArea.textContent);
  operation = this.textContent;

  outputArea.textContent += ` ${this.textContent} `;
  outputArea = document.querySelector("#operand-2");
}

function getOutput() {
  secondOperand = Number(outputArea.textContent);
  let temp = outputArea.textContent + ` ${this.textContent} `;
  outputArea = document.querySelector("#operand-1");
  outputArea.textContent += ` ${temp}`;

  switch (operation.toLocaleUpperCase()) {
    case "+".toLocaleUpperCase():
      outputValue = firstOperand + secondOperand;
      break;
    case "-".toLocaleUpperCase():
      outputValue = firstOperand - secondOperand;
      break;
    case "*".toLocaleUpperCase():
      outputValue = firstOperand * secondOperand;
      break;
    case "/".toLocaleUpperCase():
      outputValue = firstOperand / secondOperand;
      break;
    case "x2".toLocaleUpperCase():
      outputValue = Math.pow(firstOperand, 2);
      break;
    case "xy".toLocaleUpperCase():
      outputValue = Math.pow(firstOperand, secondOperand);
      break;
    default:
      outputValue = "Err";
  }
  outputArea = document.querySelector("#operand-2");
  outputArea.textContent = outputValue;
}

function resetAll() {
  firstOperand = "";
  secondOperand = "";
  operation = "";
  outputArea.textContent = "";
  outputArea = document.querySelector("#operand-1");
  outputArea.textContent = "0";
}
