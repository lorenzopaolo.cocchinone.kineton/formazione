const playerTurnValues = document.querySelectorAll(".player-turn-value");
const allBox = document.querySelectorAll(".content-value");

const startBtn = document.querySelector(".start-btn");
const resetBtn = document.querySelector(".reset-btn");

startBtn.addEventListener("click", startGame);
resetBtn.addEventListener("click", resetAll);

let gameValue = "";
let isWin = false;
let winner = "";

function startGame() {
  const randPlayerChoice = Math.floor(Math.random() * 2);
  playerTurnValues[randPlayerChoice].style.display = null;
  gameValue = playerTurnValues[randPlayerChoice].id;
  allBox.forEach((box) => {
    box.addEventListener("click", gameLogic);
  });
}

function gameLogic() {
  this.textContent = gameValue;
  if (gameValue.toLocaleUpperCase() == "X") {
    gameValue = "O";
    playerTurnValues[0].style.display = "none";
    playerTurnValues[1].style.display = null;
  } else if (gameValue.toLocaleUpperCase() == "O") {
    gameValue = "X";
    playerTurnValues[0].style.display = null;
    playerTurnValues[1].style.display = "none";
  }

  winCondition();
}

function resetAll() {
  allBox.forEach((box) => {
    box.textContent = "";
  });

  playerTurnValues.forEach((value) => {
    value.style.display = "none";
  });

  winner = "";
  isWin = false;
  gameValue = "";
}

function winCondition() {
  let board = [];
  allBox.forEach((box) => {
    board.push(box.textContent);
  });

  //row
  if (board[0] != "" && board[1] != "" && board[2] != "") {
    if (board[0] == board[1] && board[0] == board[2]) {
      winner = board[0];
      isWin = true;
      console.log(`1° - ${winner} ha vinto il gioco`);
    }
  }
  if (board[3] != "" && board[4] != "" && board[5] != "") {
    if (board[3] == board[4] && board[3] == board[5]) {
      winner = board[3];
      isWin = true;
      console.log(`2° - ${winner} ha vinto il gioco`);
    }
  }
  if (board[6] != "" && board[7] != "" && board[8] != "") {
    if (board[6] == board[7] && board[6] == board[8]) {
      winner = board[6];
      isWin = true;
      console.log(`3° - ${winner} ha vinto il gioco`);
    }
  }

  //col
  if (board[0] != "" && board[3] != "" && board[6] != "") {
    if (board[0] == board[3] && board[0] == board[6]) {
      winner = board[0];
      isWin = true;
      console.log(`4° - ${winner} ha vinto il gioco`);
    }
  }
  if (board[1] != "" && board[4] != "" && board[7] != "") {
    if (board[1] == board[4] && board[1] == board[7]) {
      winner = board[1];
      isWin = true;
      console.log(`5° - ${winner} ha vinto il gioco`);
    }
  }
  if (board[2] != "" && board[5] != "" && board[8] != "") {
    if (board[2] == board[5] && board[2] == board[8]) {
      winner = board[2];
      isWin = true;
      console.log(`6° - ${winner} ha vinto il gioco`);
    }
  }

  //dia
  if (board[0] != "" && board[4] != "" && board[8] != "") {
    if (board[0] == board[4] && board[0] == board[8]) {
      winner = board[0];
      isWin = true;
      console.log(`7° - ${winner} ha vinto il gioco`);
    }
  }
  if (board[2] != "" && board[4] != "" && board[6] != "") {
    if (board[2] == board[4] && board[2] == board[6]) {
      winner = board[2];
      isWin = true;
      console.log(`8° - ${winner} ha vinto il gioco`);
    }
  }
}

setInterval(() => {
    if (isWin) {
    const winnerPlayerEl = document.querySelector(".winner-player");
    winnerPlayerEl.style.display = "";
    winnerPlayerEl.textContent = `Ha vinto il giocatore ${winner}`;
    playerTurnValues.forEach((ele) =>{
        ele.style.display = "none";
    });
    allBox.forEach((box) => {
        box.removeEventListener("click", gameLogic);
      });
  }
}, 500);
