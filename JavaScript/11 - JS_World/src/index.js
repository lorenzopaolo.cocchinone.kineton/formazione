import { apiUrl } from "./api.js";

const showCountryBtn = document.querySelector(".show-country-button");
showCountryBtn.addEventListener("click", fetchCountry);

const showCityBtn = document.querySelector(".show-city-button");
showCityBtn.addEventListener("click", fetchCity);

const showCountryLanguageBtn = document.querySelector(".show-countrylanguage-button");
showCountryLanguageBtn.addEventListener("click", fetchCountryLanguage);


async function fetchCountry() {
  showCountryBtn.setAttribute(`aria-busy`, true);
  
  const headers = new Headers();

  headers.append("Content-Type", "text/xml");
  headers.append("Accept", "*/*");
  headers.append("Connection", "keep-alive");

  let response = await fetch(apiUrl+`/country`, {
    method: "get",
    mode: "cors",
    headers,
  }).then((response) => {
    if (!response.ok) {
      throw new Error("`HTTP error! Status: ${response.status}`");
    }

    return response.json();
  });

  const countryTable = document.querySelector(".country");
  const countryDiv = countryTable.parentElement.parentElement;
  countryDiv.classList.add("show");
  const coloText = Object.keys(response[0]);
  const headTable = document.createElement("thead");
  const headTr = document.createElement("tr");

  coloText.forEach((key) => {
    const headName = document.createElement("td");
    headName.textContent = key;
    headTr.appendChild(headName);
  });

  headTable.appendChild(headTr);
  countryTable.appendChild(headTable);

  const tableBody = document.createElement("tbody");

  response.forEach((row) => {
    let rowElement = document.createElement("tr");
    let rowData = Object.values(row);
    rowData.forEach((data) => {
      let rowTd = document.createElement("td");
      rowTd.textContent = data;
      rowElement.appendChild(rowTd);
    });
    tableBody.appendChild(rowElement);
  });

  countryTable.appendChild(tableBody);
  showCountryBtn.setAttribute(`aria-busy`, false);
}

async function fetchCity() {
  showCityBtn.setAttribute(`aria-busy`, true);
  
  const headers = new Headers();

  headers.append("Content-Type", "text/xml");
  headers.append("Accept", "*/*");
  headers.append("Connection", "keep-alive");

  let response = await fetch(apiUrl+`/city`, {
    method: "get",
    mode: "cors",
    headers,
  }).then((response) => {
    if (!response.ok) {
      throw new Error("`HTTP error! Status: ${response.status}`");
    }

    return response.json();
  });

  const countryTable = document.querySelector(".city");
  const cityDiv = countryTable.parentElement.parentElement;
  cityDiv.classList.add("show");
  const coloText = Object.keys(response[0]);
  const headTable = document.createElement("thead");
  const headTr = document.createElement("tr");

  coloText.forEach((key) => {
    const headName = document.createElement("td");
    headName.textContent = key;
    headTr.appendChild(headName);
  });

  headTable.appendChild(headTr);
  countryTable.appendChild(headTable);

  const tableBody = document.createElement("tbody");

  response.forEach((row) => {
    let rowElement = document.createElement("tr");
    let rowData = Object.values(row);
    rowData.forEach((data) => {
      let rowTd = document.createElement("td");
      rowTd.textContent = data;
      rowElement.appendChild(rowTd);
    });
    tableBody.appendChild(rowElement);
  });

  countryTable.appendChild(tableBody);
  showCityBtn.setAttribute(`aria-busy`, false);
}

async function fetchCountryLanguage() {
  showCountryLanguageBtn.setAttribute(`aria-busy`, true);
  
  const headers = new Headers();

  headers.append("Content-Type", "text/xml");
  headers.append("Accept", "*/*");
  headers.append("Connection", "keep-alive");

  let response = await fetch(apiUrl+`/language`, {
    method: "get",
    mode: "cors",
    headers,
  }).then((response) => {
    if (!response.ok) {
      throw new Error("`HTTP error! Status: ${response.status}`");
    }

    return response.json();
  });

  const countryTable = document.querySelector(".country-language");
  const countryLanguageDiv = countryTable.parentElement.parentElement;
  countryLanguageDiv.classList.add("show");
  const coloText = Object.keys(response[0]);
  const headTable = document.createElement("thead");
  const headTr = document.createElement("tr");

  coloText.forEach((key) => {
    const headName = document.createElement("td");
    headName.textContent = key;
    headTr.appendChild(headName);
  });

  headTable.appendChild(headTr);
  countryTable.appendChild(headTable);

  const tableBody = document.createElement("tbody");

  response.forEach((row) => {
    let rowElement = document.createElement("tr");
    let rowData = Object.values(row);
    rowData.forEach((data) => {
      let rowTd = document.createElement("td");
      rowTd.textContent = data;
      rowElement.appendChild(rowTd);
    });
    tableBody.appendChild(rowElement);
  });

  countryTable.appendChild(tableBody);
  showCountryLanguageBtn.setAttribute(`aria-busy`, false);
}
