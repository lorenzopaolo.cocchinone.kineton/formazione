const btnShow = document.querySelectorAll(".expand");

function manageContent() {
    
    const indexMenu = this.id.slice(4,5);
    const content = document.querySelector(`.content-${indexMenu}`);
    if(!content.classList.contains("show")){
        content.classList.add("show");
        return;
    }

    content.classList.remove("show");
    
}

btnShow.forEach((btn) => btn.addEventListener("click", manageContent));
