import { createTable } from "./controller.js";
import { showingTable } from "./controller.js";
import { hidingTable } from "./controller.js";

const drawTableBtn = document.querySelector(".draw-table");
drawTableBtn.addEventListener("click", createTable);

const viewTableBtn = document.querySelector(".view-table");
viewTableBtn.addEventListener("click", showingTable);

const hideTableBtn = document.querySelector(".hide-table");
hideTableBtn.addEventListener("click", hidingTable);
