import { data } from "./model.js";
import { drawTable } from "./view.js";
import { showTable } from "./view.js";
import { hideTable } from "./view.js";

export function createTable() {
  const tableElement = document.createElement("table");
  tableElement.classList.add("table");
  tableElement.classList.add("table-striped");
  tableElement.classList.add("employee-table");

  const tableHeader = document.createElement("thead");
  const tableBody = document.createElement("tbody");

  //Creazione delle Colonne
  const titles = document.createElement("tr");

  const colTitles = Object.keys(data[0]);

  colTitles.forEach((element) => {
    let colTitle = document.createElement("td");
    let iElement = document.createElement("i");
    let spanElement = document.createElement("span");

    iElement.classList.add("fa-solid");

    switch (element.toUpperCase()) {
      case "id".toUpperCase():
        spanElement.textContent = " " + "ID";
        iElement.classList.add("fa-hashtag");
        break;

      case "first_name".toUpperCase():
        spanElement.textContent = " " + "First Name";
        iElement.classList.add("fa-user");
        break;
      
      case "last_name".toUpperCase() :
        spanElement.textContent = " " + "Last Name";
        iElement.classList.add("fa-user");
        break;

      case "email".toUpperCase():
        spanElement.textContent = " " + "E-Mail";
        iElement.classList.add("fa-envelope");
        break;

      case "gender".toUpperCase():
        spanElement.textContent = " " + "Gender";
        iElement.classList.add("fa-person");
        break;

      case "street_address".toUpperCase():
        spanElement.textContent = " " + "Street Address";
        iElement.classList.add("fa-road");
        break;
      
        default:
          spanElement.textContent = " " + element;
          iElement.classList.add("fa-cookie-bite");
    }


    

    colTitle.appendChild(iElement);
    colTitle.appendChild(spanElement);

    titles.appendChild(colTitle);
  });

  //Creazione delle righe
  data.forEach((employee) => {
    let row = document.createElement("tr");

    for (let i = 0; i < Object.values(employee).length; i++) {
      let rowData = document.createElement("td");
      let idEmployee = Object.values(employee)[i];

      if (i == 0) {
        let digits = countDigit(idEmployee);
        for (let z = 0; z <= digits; z++) {
          let stringDigit = String(idEmployee);
          let iElement = document.createElement("i");
          iElement.classList.add("fa-solid");
          iElement.classList.add(`fa-${stringDigit[z]}`);
          rowData.appendChild(iElement);
        }
      } else {
        rowData.textContent = Object.values(employee)[i];
      }

      row.appendChild(rowData);
    }

    tableBody.appendChild(row);
  });

  tableHeader.appendChild(titles);

  tableElement.appendChild(tableHeader);
  tableElement.appendChild(tableBody);

  drawTable(tableElement);
}

export function showingTable() {
  const tableElement = document.querySelector(".employee-table");
  showTable(tableElement);
}

export function hidingTable() {
  const tableElement = document.querySelector(".employee-table");
  hideTable(tableElement);
}

// Utils
function countDigit(n) {
  return Math.floor(Math.log10(n) + 1);
}
