export function drawTable(table) {
  const drawTableBtn = document.querySelector(".draw-table");
  drawTableBtn.style.display = "none";

  const viewTableBtn = document.querySelector(".view-table");
  viewTableBtn.style.display = "";
  viewTableBtn.setAttribute("disabled", true);

  const hideTableBtn = document.querySelector(".hide-table");
  hideTableBtn.style.display = "";
  hideTableBtn.removeAttribute("disabled");

  const rootDiv = document.querySelector(".container");
  rootDiv.appendChild(table);
}

export function showTable(table) {
  const viewTableBtn = document.querySelector(".view-table");
  viewTableBtn.setAttribute("disabled", true);

  const hideTableBtn = document.querySelector(".hide-table");
  hideTableBtn.removeAttribute("disabled");
  table.style.display = "";
}

export function hideTable(table) {
  const viewTableBtn = document.querySelector(".view-table");
  viewTableBtn.removeAttribute("disabled");

  const hideTableBtn = document.querySelector(".hide-table");
  hideTableBtn.setAttribute("disabled", true);
  table.style.display = "none";
}
