import { createMenu } from "./menu/menu_controller.js";
import { createImg } from "./image/image_controler.js";
import { createLogo } from "./clock/clock-container.js";
import { createClock } from "./clock/clock-container.js";


createMenu();
createImg();
createLogo();
createClock();
