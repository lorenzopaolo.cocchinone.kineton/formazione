export function drawLogo(logo){
    const divImage = document.querySelector(".clock-logo");

    divImage.append(logo);
}

export function drawClock(clock){
    const divImage = document.querySelector(".clock-logo");
    if(divImage.children[1] == undefined){
        divImage.append(clock);
    }else {
        divImage.replaceChild(clock, divImage.children[1] );
    }
}
