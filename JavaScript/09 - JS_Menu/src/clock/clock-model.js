export const image = {
    src: `resources/images/sky-logo.png`,
    alt: `sky-logo`,
    width: `50px`,
    height: `25px`,
}