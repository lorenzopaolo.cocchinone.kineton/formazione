import { image } from "./clock-model.js";
import { drawClock, drawLogo } from "./clock-view.js";

export function createLogo(){
    const imageComp = document.createElement("img");
    imageComp.classList.add("img-fluid");

    imageComp.setAttribute("src", image.src);
    imageComp.setAttribute("alt", image.alt);
    imageComp.setAttribute("width", image.width);
    imageComp.setAttribute("height", image.height);

    drawLogo(imageComp);

}

export function createClock(){
    const spanClock = document.createElement("span");
    spanClock.classList.add("text-white");
    spanClock.classList.add("clock");


    const today = new Date();
    let hours = String(today.getHours());
    let minutes = String(today.getMinutes());

    let firstDig = document.createElement("i");
    firstDig.classList.add("fa.solid");

    let secondDig = document.createElement("i");
    secondDig.classList.add("fa.solid");

    let thirdDig = document.createElement("i");
    thirdDig.classList.add("fa.solid");

    let fourthDig = document.createElement("i");
    fourthDig.classList.add("fa.solid");

    if(hours.length < 2){
        firstDig.classList.add("fa-0");
        secondDig.classList.add(`fa-${hours[0]}`);
    }else{
        firstDig.classList.add(`fa-${hours[0]}`);
        secondDig.classList.add(`fa-${hours[1]}`);
    }

    if(minutes.length < 2){
        thirdDig.classList.add("fa-0");
        fourthDig.classList.add(`fa-${minutes[0]}`);
    }else{
        thirdDig.classList.add(`fa-${minutes[0]}`);
        fourthDig.classList.add(`fa-${minutes[1]}`);
    }

    secondDig.textContent = " : ";

    spanClock.appendChild(firstDig)
    spanClock.appendChild(secondDig);
    spanClock.appendChild(thirdDig);
    spanClock.appendChild(fourthDig);

    spanClock.style.paddingLeft = "25px";
    drawClock(spanClock);

    setTimeout(createClock, 10000);
}