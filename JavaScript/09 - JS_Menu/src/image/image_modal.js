export const image = {
    src: `resources/images/s-image.jpg`,
    alt: `s-image`,
    width: `500px`,
    height: `250px`,
}