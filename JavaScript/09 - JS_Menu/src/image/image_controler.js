import { image } from "./image_modal.js";
import { drawImg } from "./image_view.js";

export function createImg(){
    const imageComp = document.createElement("img");
    imageComp.classList.add("img-fluid");
    imageComp.classList.add("border");
    imageComp.classList.add("border-primary");
    imageComp.classList.add("rounded");

    imageComp.setAttribute("src", image.src);
    imageComp.setAttribute("alt", image.alt);
    imageComp.setAttribute("width", image.width);
    imageComp.setAttribute("height", image.height);

    drawImg(imageComp);

}