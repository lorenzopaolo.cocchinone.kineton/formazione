import { menuData } from "./menu_model.js";
import { drawMenu} from "./menu_view.js";


export function createMenu(){
    const titleMenu = document.createElement("h3");
    titleMenu.classList.add("text-white");

    const titleMenuIEl = document.createElement("i");
    titleMenuIEl.classList.add("bi");
    titleMenuIEl.classList.add("bi-gear");

    const titleMenuSpan = document.createElement("span");
    titleMenuSpan.textContent = " Impostazioni";

    titleMenu.appendChild(titleMenuIEl);
    titleMenu.appendChild(titleMenuSpan)

    const menu = document.createElement("ul");

    menu.classList.add("nav");
    menu.classList.add("nav-pills");
    menu.classList.add("flex-column");
    menu.classList.add("mb-auto");

    menuData.forEach((entry) => {
        const entryMenu = document.createElement("li");

        const entryMenuData = document.createElement("p");

        entryMenuData.style.marginBottom = '5px';
        entryMenuData.classList.add("nav-link");
        entryMenuData.classList.add("text-white");

        const svgEntry = document.createElement("svg");
        svgEntry.classList.add("bi");
        svgEntry.classList.add("pe-none");
        svgEntry.classList.add("me-2");
        svgEntry.setAttribute("width", 16);
        svgEntry.setAttribute("height", 16);

        let iElement = document.createElement("i");
        iElement.classList.add("bi");

        switch (entry.toUpperCase()){
            case "Generali".toUpperCase():
                iElement.classList.add("bi-house-gear");
                break;

            case "Info".toUpperCase():
                iElement.classList.add("bi-info-circle");
                break;

            case "Parental".toUpperCase():
                iElement.classList.add("bi-person-circle");
                break;
            
            case "Accessibilità".toUpperCase():
                iElement.classList.add("bi-universal-access-circle");
                break;
            
            case "Configura".toUpperCase():
                iElement.classList.add("bi-gear-wide-connected");
                break;
            
            case "Diagnostica".toUpperCase():
                iElement.classList.add("bi-heart-pulse");
                break;
            
            case "Digitale Terrestre".toUpperCase():
                iElement.classList.add("bi-reception-3");
                break;

            case "Altri canali SAT".toUpperCase():
                iElement.classList.add("bi-tv");
                break;
        }

        const spanEntry = document.createElement("span");
        spanEntry.textContent = " " + entry;

        entryMenuData.appendChild(svgEntry);
        entryMenuData.appendChild(iElement);
        entryMenuData.appendChild(spanEntry);

        entryMenu.appendChild(entryMenuData);

        menu.appendChild(entryMenu);

    });

    drawMenu(menu, titleMenu);

}

