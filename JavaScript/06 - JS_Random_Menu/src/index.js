'use strict';
//Fuction Declaration

function getRandom(max){
    return Math.floor(Math.random() * max) + 1;
}

function createExternalDiv(entryNumber){
    let divEntry = document.createElement("div");
    divEntry.classList.add(`entry-${entryNumber}`);
    return divEntry;
}

function createInternalDiv(entryNumber){
    let divEntry = document.createElement("div");
    divEntry.classList.add(`submenu-${entryNumber}`);
    divEntry.style.display = "none";
    return divEntry;
}

function createTitleMenu(entryMenu){
    let titleMenu = document.createElement("h3");
    titleMenu.id = `title-${entryMenu}`;
    titleMenu.classList.add(`title`);
    return titleMenu;
}

function createButtonExpandMenu(entryButton){
    let buttonExpand = document.createElement("button");
    buttonExpand.classList.add("expand");
    buttonExpand.id = `btn-${entryButton}`;
    buttonExpand.textContent = ">";
    return buttonExpand;
}

function createContent(entryContent){
    let contentP = document.createElement("p");
    contentP.classList.add("content");
    contentP.id = `content-${entryContent}`;
    contentP.textContent = `Contenuto ${entryContent}`;
    return contentP;
}

function manageContent() {
    const indexMenu = this.id.slice(this.id.indexOf('-') + 1, this.id.length);
    const subMenu = document.querySelector(`.submenu-${indexMenu}`);
    if(!subMenu.classList.contains("show")){
        subMenu.classList.add("show");
        subMenu.style.display = null;
        return;
    }
    subMenu.classList.remove("show");
    subMenu.style.display = "none";
}

function createMenu() {
    for(let i = 1; i <= titleRandomNumber; i++){
        //Create Elements
        let externalDiv = createExternalDiv(i);
        let titleMenu = createTitleMenu(i);
        let buttonExpand = createButtonExpandMenu(i)
        let internalDiv = createInternalDiv(i);
        
        //Menu Entry
        titleMenu.appendChild(buttonExpand);
        titleMenu.innerHTML += ` Titolo ${i}`;
        externalDiv.appendChild(titleMenu);
    
        //Content Entry
        let randomContentEntry = getRandom(MAX_CONTENT_MENU);
        for(let j = 1; j <= randomContentEntry; j++){
            internalDiv.appendChild(createContent(j));
        }
        externalDiv.appendChild(internalDiv);
    
        //Append to Menu
        sectionMenu.appendChild(externalDiv);
    }

}

// Main Code

const MAX_ENTRY_MENU = 100;
const MAX_CONTENT_MENU = 50;

const sectionMenu = document.querySelector(".menu");
const titleRandomNumber = getRandom(MAX_ENTRY_MENU);

createMenu();

const allBtn = document.querySelectorAll('.expand');

allBtn.forEach((btn) => {
    btn.addEventListener("click", manageContent);
});


//Recuperare la posizione del titolo e del contenuto nel dom, possibilità di loggare o di fare l'allert

const allTitle = document.querySelectorAll('.title');
const allContent = document.querySelectorAll('.content');

allTitle.forEach((title) => {
    title.addEventListener("click", function (){
        let titleNumber = title.id.slice(title.id.indexOf('-')+1, title.id.length);
        console.log(`Click effettuato sul Titolo n° ${titleNumber}`);
    });
});

allContent.forEach((content) => {
    content.addEventListener("click", function (){
        let contentNumber = content.id.slice(content.id.indexOf('-') + 1, content.id.length);

        let parentNodeClassList = content.parentNode.classList;
        let subMenuNumber = parentNodeClassList.value.slice(parentNodeClassList.value.indexOf('-') + 1, 
                                parentNodeClassList.value.indexOf(' '));
        
        console.log(`Click effettuato sul Contenuto numero° ${contentNumber} del Titolo numero: ${subMenuNumber}` );
        
    });
});
