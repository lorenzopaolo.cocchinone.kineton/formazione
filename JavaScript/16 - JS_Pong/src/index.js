const WIDTH = 650;
const HEIGHT = 300;
const GAME = {};

let ARROW_UP = "ArrowUp";
let ARROW_DOWN = "ArrowDown";

const PLAYER = {
  rectHeight: 50,
  rectWidth: 8,
  actualPosition: { x: 0, y: 0 },
};

const ENEMY = {
  rectHeight: 50,
  rectWidth: 8,
  actualPosition: { x: 0, y: 0 },
};

const BALL = {
  speed: 5,
  radius: 5,
  startAngle: 0,
  endAngle: 2 * Math.PI,
  actualPosition: { x: 0, y: 0 },
  direction: '-x',
};

const PLAYER_SPEED = 5;
const ENEMY_SPEED = 5;
const OFFSET_POSITION = 28;

const canvas = document.querySelector(".game");
canvas.setAttribute("width", WIDTH);
canvas.setAttribute("height", HEIGHT);
const canvasContext = canvas.getContext("2d");
canvasContext.fillStyle = "white";
canvasContext.beginPath();

function drawNet(canvasContext) {
  // Net
  let rectHeightDraw = -25;
  let i = 0;
  do {
    canvasContext.fillRect(WIDTH / 2, (rectHeightDraw += 30), 5, 20);
    i += 1;
  } while (i < 12);
}

function drawPlayer(canvasContext, posX, posY, offset = 0) {
  canvasContext.fillRect(
    posX,
    posY + offset,
    PLAYER.rectWidth,
    PLAYER.rectHeight
  );
  PLAYER.actualPosition.x = posX;
  PLAYER.actualPosition.y = posY + offset;
}

function drawEnemy(canvasContext, posX, posY, offset = 0) {
  canvasContext.fillRect(
    posX,
    posY + offset,
    ENEMY.rectWidth,
    ENEMY.rectHeight
  );

  ENEMY.actualPosition.x = posX;
  ENEMY.actualPosition.y = posY + offset;
}

function drawBall(canvasContext, posX, posY) {
  canvasContext.beginPath();
  if (BALL.direction == 'x') {
    canvasContext.arc(posX += BALL.speed, posY, BALL.radius, BALL.startAngle, BALL.endAngle);
  }

  if (BALL.direction == '-x') {
    canvasContext.arc(posX -= BALL.speed, posY, BALL.radius, BALL.startAngle, BALL.endAngle);
  }

  if (BALL.direction == 'x-y') {
    canvasContext.arc(posX += BALL.speed / 2, posY -= BALL.speed / 2, BALL.radius, BALL.startAngle, BALL.endAngle);
  }

  if (BALL.direction == 'xy') {
    canvasContext.arc(posX += BALL.speed / 2, posY += BALL.speed / 2, BALL.radius, BALL.startAngle, BALL.endAngle);
  }

  if (BALL.direction == '-x-y') {
    canvasContext.arc(posX -= BALL.speed / 2, posY -= BALL.speed / 2, BALL.radius, BALL.startAngle, BALL.endAngle);
  }

  if (BALL.direction == '-xy') {
    canvasContext.arc(posX -= BALL.speed / 2, posY += BALL.speed / 2, BALL.radius, BALL.startAngle, BALL.endAngle);
  }



  //Stroke Rect for Collissions
  canvasContext.strokeStyle = "#6a6868";
  canvasContext.strokeRect(
    posX - BALL.radius,
    posY - BALL.radius,
    BALL.radius * 2,
    BALL.radius * 2
  );

  BALL.actualPosition.x = posX;
  BALL.actualPosition.y = posY;

  canvasContext.fill();
  canvasContext.closePath();
}

function clearBall(canvasContext, posX, posY) {
  canvasContext.clearRect(
    posX - BALL.radius,
    posY - BALL.radius,
    BALL.radius * 2,
    BALL.radius * 2
  );
  canvasContext.clearRect(
    posX - BALL.radius - 2,
    posY - BALL.radius - 2,
    BALL.radius * 2 + 3,
    BALL.radius * 2 + 3
  );
}

function addPlayerMovementEvent(canvasContext) {
  document.addEventListener("keydown", (keyboardEvent) => {
    playerMovement(keyboardEvent, canvasContext);
  });
}

function playerMovement(keyboardEvent, canvasContext) {
  if (keyboardEvent.key == ARROW_UP) {
    canvasContext.clearRect(
      PLAYER.actualPosition.x,
      PLAYER.actualPosition.y,
      PLAYER.rectWidth,
      PLAYER.rectHeight
    );
    drawPlayer(
      canvasContext,
      PLAYER.actualPosition.x,
      PLAYER.actualPosition.y - PLAYER_SPEED
    );
  }

  if (keyboardEvent.key == ARROW_DOWN) {
    canvasContext.clearRect(
      PLAYER.actualPosition.x,
      PLAYER.actualPosition.y,
      PLAYER.rectWidth,
      PLAYER.rectHeight
    );
    drawPlayer(
      canvasContext,
      PLAYER.actualPosition.x,
      PLAYER.actualPosition.y + PLAYER_SPEED
    );
  }
}

function update(canvasContext) {
  clearBall(
    canvasContext,
    BALL.actualPosition.x,
    BALL.actualPosition.y,
    BALL.radius
  );
  drawBall(
    canvasContext,
    BALL.actualPosition.x,
    BALL.actualPosition.y
  );

  // Collision Detection Player
  for (let i = PLAYER.actualPosition.y; i <= PLAYER.actualPosition.y + PLAYER.rectHeight; i++) {
    for (let k = PLAYER.actualPosition.x; k <= PLAYER.actualPosition.x + PLAYER.rectWidth; k++) {
      // With Ball
      if (k == BALL.actualPosition.x - BALL.radius * 2 && i == BALL.actualPosition.y) {
        let collissionPosition = (BALL.actualPosition.y - PLAYER.actualPosition.y) / 50 * 100;

        if (collissionPosition < 35) {
          BALL.direction = 'x-y';
        }
        if (collissionPosition >= 35 && collissionPosition < 65) {
          BALL.direction = 'x';
        }
        if (collissionPosition >= 65) {
          BALL.direction = 'xy';
        }

      }
    }

  }

  if (PLAYER.actualPosition.y <= 0) {
    ARROW_UP = '';
  } else {
    ARROW_UP = 'ArrowUp';
  }

  if (PLAYER.actualPosition.y + PLAYER.rectHeight >= HEIGHT) {
    ARROW_DOWN = '';
  } else {
    ARROW_DOWN = 'ArrowDown';
  }


  // Collision Detection Enemy
  for (let i = ENEMY.actualPosition.y; i <= ENEMY.actualPosition.y + ENEMY.rectHeight; i++) {
    for (let k = ENEMY.actualPosition.x; k <= ENEMY.actualPosition.x; k++) {
      // With Ball
      if (k == BALL.actualPosition.x + BALL.radius * 2 && i == BALL.actualPosition.y) {
        BALL.direction = '-x';
        let collissionPosition = (BALL.actualPosition.y - PLAYER.actualPosition.y) / 50 * 100;

        if (collissionPosition < 35) {
          BALL.direction = '-x-y';
        }
        if (collissionPosition >= 35 && collissionPosition < 65) {
          BALL.direction = '-x';
        }
        if (collissionPosition >= 65) {
          BALL.direction = '-xy';
        }
      }
    }
  }

  // Ball Collision
  if (BALL.actualPosition.y - 5 < 0) {
    if (BALL.direction == 'x-y') {
      BALL.direction = 'xy';
    }
    if (BALL.direction == '-x-y') {
      BALL.direction = '-xy';
    }

  }

  if (BALL.actualPosition.y + BALL.radius * 2 + 5 > HEIGHT) {
    if (BALL.direction == 'xy') {
      BALL.direction = 'x-y';
    }
    if (BALL.direction == '-xy') {
      BALL.direction = '-x-y';
    }

  }

  if (BALL.actualPosition.x > WIDTH / 2 && ENEMY.actualPosition.y + ENEMY.rectHeight / 2 != BALL.actualPosition.y) {

    if (ENEMY.actualPosition.y + ENEMY.rectHeight / 2 >= BALL.actualPosition.y) {
      canvasContext.clearRect(
        ENEMY.actualPosition.x,
        ENEMY.actualPosition.y,
        ENEMY.rectWidth,
        ENEMY.rectHeight
      );
      drawEnemy(
        canvasContext,
        ENEMY.actualPosition.x,
        ENEMY.actualPosition.y - ENEMY_SPEED
      );
    }
    if (ENEMY.actualPosition.y + ENEMY.rectHeight / 2 < BALL.actualPosition.y) {
      canvasContext.clearRect(
        ENEMY.actualPosition.x,
        ENEMY.actualPosition.y,
        ENEMY.rectWidth,
        ENEMY.rectHeight
      );
      drawEnemy(
        canvasContext,
        ENEMY.actualPosition.x,
        ENEMY.actualPosition.y + ENEMY_SPEED
      );
    }


  }


}

drawNet(canvasContext);
drawEnemy(
  canvasContext,
  WIDTH - 25,
  HEIGHT / 2 - ENEMY.rectHeight,
  OFFSET_POSITION
);
drawPlayer(canvasContext, 25, HEIGHT / 2 - PLAYER.rectHeight, OFFSET_POSITION);
drawBall(canvasContext, WIDTH / 2 - 50, HEIGHT / 2);
addPlayerMovementEvent(canvasContext);

setInterval(() => {

}, 17);

//Game Loop
(() => {
  function gameloop(tFrame) {
    GAME.stopLoop = window.requestAnimationFrame(gameloop);

    update(canvasContext);
  }

  gameloop();
})();