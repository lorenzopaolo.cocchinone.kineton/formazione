import { newUser, getTenUsers, getUserById } from "./fetch.js";

const simpleForm = document.querySelector("#simple-form");
const getTenUserBtn = document.querySelector(".get-ten-users");
const getUserByIdForm = document.querySelector("#user-by-id-form");

simpleForm.onsubmit = async (e) => {
    e.preventDefault();
    simpleForm.submit.setAttribute("aria-busy", true);

    newUser(new FormData(simpleForm)).then((response) => {
        console.log(response);
    });

    simpleForm.submit.setAttribute("aria-busy", false);
};

getTenUserBtn.onclick = (e) => {
    getTenUserBtn.setAttribute("aria-busy", true);

    getTenUsers().then((response) => {
        console.log(response);
    });

    getTenUserBtn.setAttribute("aria-busy", false);
}


getUserByIdForm.onsubmit = (e) => {
    e.preventDefault();
    getUserByIdForm.submit.setAttribute("aria-busy", true);


    getUserById(getUserByIdForm.id.value).then((response) => {
        console.log(response);
    });

    getUserByIdForm.submit.setAttribute("aria-busy", false);
}

