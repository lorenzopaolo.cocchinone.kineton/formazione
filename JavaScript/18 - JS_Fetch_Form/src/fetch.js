import { apiURL } from "./serverAPI.js";


export async function newUser(formData){
    const jsonObject = {};

    formData.forEach((value, key) => {
        jsonObject[key] = value;
    });

    let request = await fetch(`${apiURL}/`,{
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        mode: 'cors',
        body: JSON.stringify(jsonObject),
    });

    let response = await request.json();
    return response;

}

export async function getTenUsers(){
    let request = await fetch(`${apiURL}/users`, {
        headers: new Headers({
            "ngrok-skip-browser-warning": "69420",
          }),
        method: 'GET',
        mode: 'cors',
    });

    let response = await request.json();
    return response;    
}

export async function getUserById(id){
    let request = await fetch(`${apiURL}/user/${id}`, {
        method: 'GET',
        mode: 'cors',
    });

    let response = await request.json();
    return response;

}