


function isModalOpen(id){
    const modals = document.querySelectorAll(".modal");

    modals.forEach((modal) => {
        if(modal.id == id){
            return (modal.hasAttribute("open") && modal.getAttribute("open")) ? true : false;
        }
    });
}

console.log(isModalOpen("signin-modal"));