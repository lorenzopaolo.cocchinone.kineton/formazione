
const SPACE = {};
const WIDTH = 800;
const HEIGHT = 600;
const SPACE_BAR = 'Space';
const KEY_F = 'KeyF';
let LEFT = 'ArrowLeft';
let RIGHT = 'ArrowRight';

const canvas = document.querySelector(".game");
canvas.setAttribute("width", WIDTH);
canvas.setAttribute("height", HEIGHT);

const canvasContext = canvas.getContext("2d");
canvasContext.fillStyle = 'white';
canvasContext.strokeStyle = "slategray";//"slategray";
canvasContext.font = "48px serif";

const fpsElement = document.querySelector(".fps");
const scoreElement = document.querySelector(".score");

const shipImage = new Image(64, 64);
shipImage.src = 'resources/ship.png';

const projectileImage = new Image(20, 40);
projectileImage.src = 'resources/rocket.png';

const specialImage = new Image(64, 64);
specialImage.src = 'resources/special.png';

const enemyImage = new Image(48, 48);
enemyImage.src = 'resources/alien.png';

const PLAYER = {
    actualPosition: {x: 0, y:0},
    speed: 5,
}

const PROJECTILE = {
    actualPosition: {x:0, y:0},
    speed: 5,
}

const SPECIAL = {
    actualPosition: {x:0, y:0},
    speed: 5,
    quantity: 5,
}

class Enemy {
    constructor() {
        this.actualPosition = { x: 0, y: 0 };
        this.hitPoint = 3;
    }
}

const ENEMY = {
    speed: 0.5,
}

const ENEMIES = [];

let canShoot = true;
let isShootingNormal = false;
let isShootingSpecial = false;
let score = 0;




// Player
function drawPlayerImage(){
    canvasContext.drawImage(shipImage, PLAYER.actualPosition.x, PLAYER.actualPosition.y);
}

function drawPlayer(canvasContext, posX, posY){
    PLAYER.actualPosition.x = posX;
    PLAYER.actualPosition.y = posY;

    canvasContext.strokeRect(posX, posY, shipImage.width, shipImage.height);
    drawPlayerImage();
}

function clearPlayer(canvasContext){
    // - 1 , -1, +2, +2 is for Border that StrokeRect creates
    canvasContext.clearRect(PLAYER.actualPosition.x - 1, PLAYER.actualPosition.y - 1, shipImage.width + 2, shipImage.height + 2);

}

function addPlayerMovementEvent(canvasContext){
    document.addEventListener("keydown", function(keyboardEvent){
        playerMovement(keyboardEvent, canvasContext);
    });
}

function playerMovement(keyboardEvent, canvasContext){
    keyboardEvent.preventDefault();

    if(keyboardEvent.code == RIGHT){
        clearPlayer(canvasContext);
        drawPlayer(canvasContext, 
            PLAYER.actualPosition.x += PLAYER.speed, 
            PLAYER.actualPosition.y);

    }

    if(keyboardEvent.code == SPACE_BAR){
        if(canShoot){
            isShootingNormal = true;
            canShoot = false;
            drawProjectile(canvasContext, 
                PLAYER.actualPosition.x + (shipImage.width / 2) - (projectileImage.width / 2), 
                PLAYER.actualPosition.y - projectileImage.height - 5);

        }
    }

    if(keyboardEvent.code == KEY_F){
        if(canShoot && SPECIAL.quantity){
            isShootingSpecial = true;
            canShoot = false;
            SPECIAL.quantity -= 1;
            canvasContext.clearRect( WIDTH - 90, HEIGHT - 75, 25, 48);
            canvasContext.fillText(SPECIAL.quantity,  WIDTH - 90, HEIGHT - 30);
            drawSpecial(canvasContext, 
                PLAYER.actualPosition.x + (shipImage.width / 2) - (specialImage.width / 2), 
                PLAYER.actualPosition.y - specialImage.height - 5);
        }

        
    }

    if(keyboardEvent.code == LEFT){
        clearPlayer(canvasContext);
        drawPlayer(canvasContext, 
            PLAYER.actualPosition.x -= PLAYER.speed, 
            PLAYER.actualPosition.y);
    }
}

function playerCollision(){
    if(PLAYER.actualPosition.x <= 0 + 5){
        LEFT = '';
    } else {
        LEFT = 'ArrowLeft';
    }
    if(PLAYER.actualPosition.x + shipImage.width >= WIDTH - 5){
        RIGHT = '';
    } else {
        RIGHT = 'ArrowRight';
    }


    for(let i = 0; i < ENEMIES.length; i++){    
        for(let j = 0; j < ENEMIES[i].length; j++){
            if(ENEMIES[i][j] == null){
                continue;
            }

            if(PLAYER.actualPosition.y - 50 <= ENEMIES[i][j].actualPosition.y + enemyImage.height){
                window.cancelAnimationFrame(SPACE.stopID);
            }

        }
    }

}




// Projectile
function drawProjectile(canvasContext, posX, posY){
    PROJECTILE.actualPosition.x = posX; 
    PROJECTILE.actualPosition.y = posY;

    canvasContext.strokeRect(PROJECTILE.actualPosition.x, PROJECTILE.actualPosition.y, projectileImage.width, projectileImage.height);
    drawProjectileImage(PROJECTILE.actualPosition.x, PROJECTILE.actualPosition.y);
}

function drawProjectileImage(posX, posY){
    canvasContext.drawImage(projectileImage, posX, posY);
}

function clearProjectile(){
    canvasContext.clearRect(PROJECTILE.actualPosition.x - 1, 
        PROJECTILE.actualPosition.y - 1, 
        projectileImage.width + 2, 
        projectileImage.height + 2);

}

function projectileMoving(canvasContext){
    clearProjectile();
    drawProjectile(canvasContext, PROJECTILE.actualPosition.x, PROJECTILE.actualPosition.y -= PROJECTILE.speed);

}

function projectileCollission(canvasContext){
    //Border Collission
    if(PROJECTILE.actualPosition.y <= 5){
        clearProjectile();
        canShoot = true;
        isShootingNormal = false;
    }

    // Enemy Collission
    let p_m = PROJECTILE.actualPosition.x + projectileImage.width / 2; // Punto medio Proiettile X
    let y = PROJECTILE.actualPosition.y;

    for(let j = 0; j < ENEMIES.length; j++){
        for(let k = 0; k < ENEMIES[j].length; k++){
            if(ENEMIES[j][k] == null){
                continue;
            }

            if(p_m >=   ENEMIES[j][k].actualPosition.x && p_m <=  ENEMIES[j][k].actualPosition.x + enemyImage.width
                && y <= ENEMIES[j][k].actualPosition.y + enemyImage.height && y >= ENEMIES[j][k].actualPosition.y){
                ENEMIES[j][k].hitPoint -= 1;
                if(ENEMIES[j][k].hitPoint <= 0){
                    clearEnemy(ENEMIES[j][k]);
                    ENEMIES[j][k] = null;
                    score += 1000;
                    updateScore(canvasContext);
                }
                clearProjectile();
                canShoot = true;
                isShootingNormal = false;     
            }
        }
    }
}




// Special
function drawSpecial(canvasContext, posX, posY){
    SPECIAL.actualPosition.x = posX; 
    SPECIAL.actualPosition.y = posY;

    canvasContext.strokeRect(SPECIAL.actualPosition.x, SPECIAL.actualPosition.y, specialImage.width, specialImage.height);
    drawSpecialImage(SPECIAL.actualPosition.x, SPECIAL.actualPosition.y);
}

function drawSpecialImage(posX, posY){
    canvasContext.drawImage(specialImage, posX, posY);
}

function clearSpecial(){
    canvasContext.clearRect(SPECIAL.actualPosition.x - 1, 
        SPECIAL.actualPosition.y - 1, 
        specialImage.width + 2, 
        specialImage.height + 2);

}

function specialMoving(canvasContext){
    clearSpecial();
    drawSpecial(canvasContext, SPECIAL.actualPosition.x, SPECIAL.actualPosition.y -= SPECIAL.speed);

}

function specialCollission(canvasContext){
    //Border Collission
    if(SPECIAL.actualPosition.y <= 5){
        clearSpecial();
        canShoot = true;
        isShootingSpecial = false;
    }

    // Enemy Collission
    let p_m = SPECIAL.actualPosition.x + specialImage.width / 2;
    let y = SPECIAL.actualPosition.y;

    for(let j = 0; j < ENEMIES.length; j++){
        for(let k = 0; k < ENEMIES[j].length; k++){
            if(ENEMIES[j][k] == null){
                continue;
            }
            if(p_m >=   ENEMIES[j][k].actualPosition.x && p_m <=  ENEMIES[j][k].actualPosition.x + enemyImage.width
                && y <= ENEMIES[j][k].actualPosition.y + enemyImage.height && y >= ENEMIES[j][k].actualPosition.y){
                ENEMIES[j][k].hitPoint -= 3;
                if(ENEMIES[j][k].hitPoint <= 0){
                    clearEnemy(ENEMIES[j][k]);
                    ENEMIES[j][k] = null;
                    score += 2000;
                    updateScore(canvasContext);
                }
                clearSpecial();
                canShoot = true;
                isShootingSpecial = false;     
            }
        }
    }
}




// Enemies
function drawEnemy(canvasContext, posX, posY) {
    let enemy = new Enemy();
    enemy.actualPosition.x = posX;
    enemy.actualPosition.y = posY;


    canvasContext.strokeRect(enemy.actualPosition.x, enemy.actualPosition.y, enemyImage.width, enemyImage.width);
    drawEnemyImage(enemy.actualPosition.x, enemy.actualPosition.y);
    return enemy;
}

function drawEnemyImage(posX, posY){
    canvasContext.drawImage(enemyImage, posX, posY);

}

function drawEnemies(canvasContext, row = 3, column = 10, offset = 20){
    let posX = 0; 
    let posY = 0;

    for(let i = 0; i < row; i++){
        //Change Y
        posY +=  enemyImage.height + offset;
        ENEMIES[i] = new Array(column);
        for(let k = 0; k < column; k++){
            //Change X
            posX += enemyImage.width + offset;
            let enemy = drawEnemy(canvasContext, posX, posY);
            ENEMIES[i][k] =  enemy;
        }
        posX = 0; // Reset X for new Row
    }
}

function clearEnemy(enemy){
    canvasContext.clearRect(enemy.actualPosition.x - 1 , enemy.actualPosition.y - 10, enemyImage.width + 5, enemyImage.height + 20 );
}

function movingEnemies(canvasContext){
    if(enemiesCollission(ENEMIES[0][9]) || enemiesCollission(ENEMIES[0][0])){
        ENEMY.speed = ENEMY.speed * (-1);
        for(let i = 0; i < ENEMIES.length; i++){    
            for(let j = 0; j < ENEMIES[i].length; j++){
                if(ENEMIES[i][j] == null){
                    continue;
                }
                ENEMIES[i][j].actualPosition.y +=  5;
            }

        }
    }
    
    for(let i = 0; i < ENEMIES.length; i++){    
        for(let j = 0; j < ENEMIES[i].length; j++){
            if(ENEMIES[i][j] == null){
                continue;
            }
            ENEMIES[i][j].actualPosition.x +=  ENEMY.speed;
            clearEnemy(ENEMIES[i][j]);
            drawEnemy(canvasContext, ENEMIES[i][j].actualPosition.x, ENEMIES[i][j].actualPosition.y);
        }
    }
}

function enemiesCollission(enemy){
    if(enemy.actualPosition.x + enemyImage.width >= WIDTH - 10 || enemy.actualPosition.x <= 0 + 10 ){
        return true;
    }

    return false;

}


function updateScore(canvasContext){
    canvasContext.clearRect(40, HEIGHT - 75, 300, 50);
    canvasContext.fillText(`Score: ${score}`,  40, HEIGHT - 30);
}



drawPlayer(canvasContext, 100, 450);

shipImage.onload = drawPlayerImage;

specialImage.onload = function() {
    canvasContext.fillText(SPECIAL.quantity,  WIDTH - 90, HEIGHT - 30);
    canvasContext.fillText(`Score: ${score}`,  40, HEIGHT - 30);
    canvasContext.drawImage(specialImage, WIDTH - 75, HEIGHT - 75);
}

addPlayerMovementEvent(canvasContext);

enemyImage.onload = function() {
    drawEnemies(canvasContext);
}





let secondsPassed;
let oldTimeStamp;
let fps;

// Game Loop

(() => {
    function gameLoop(tFrame){
        SPACE.stopID = window.requestAnimationFrame(gameLoop);

        playerCollision();

        if(isShootingNormal){
            projectileMoving(canvasContext);
            projectileCollission(canvasContext);
        } 

        if(isShootingSpecial){
            specialMoving(canvasContext);
            specialCollission(canvasContext);

        }

        movingEnemies(canvasContext);
        

        //FPS Check
        secondsPassed = (tFrame - oldTimeStamp) / 1000;
        oldTimeStamp = tFrame;
        fps = Math.round(1/secondsPassed);
        fpsElement.textContent = `FPS: ${fps}`;
        
        

    }
    gameLoop();
})();
