// Const Declarations

const RED_STRING = "#ff0000";
const BLUE_STRING = "#0000ff";
const GREEN_STRING = "#008000";
const BLACK_STRING = "#000000";

/* Functions to add Activity*/
function showAddForm() {
  const addActivityBtn = document.querySelector(".activity-btn");

  addActivityBtn.addEventListener("click", addNewActivity);
  addReturnEvent("add-form");
  hideMainMenu("add-form");
}

function addNewActivity() {
  const activityDesc = document.querySelector("#activity-desc");
  const activityStart = document.querySelector("#activity-start");
  const activityFinish = document.querySelector("#activity-finish");
  const activityColor = document.querySelector("#activity-color");

  const tableBody = document.querySelector(".table-body");
  const numberActivities = [];

  for (let i = 0; i < tableBody.childElementCount; i++) {
    let stringIdChild = tableBody.children[i].id;
    numberActivities.push(
      Number(
        stringIdChild.slice(
          stringIdChild.indexOf("-") + 1,
          stringIdChild.length
        )
      )
    );
  }

  const indexOfLastActivity = maxArrayNumber(numberActivities);

  const activityRow = document.createElement("tr");
  activityRow.classList.add("content-row");
  activityRow.classList.add("drop-zone");
  activityRow.id = `content-${Number(indexOfLastActivity) + 1}`;
  activityRow.draggable = true;
  activityRow.addEventListener("drop", dropHandler);
  activityRow.addEventListener("dragover", allowDrop);
  activityRow.addEventListener("dragstart", dragHandler);

  const radioButtonActivity = document.createElement("input");
  radioButtonActivity.classList.add("form-check-input");
  radioButtonActivity.type = "radio";
  radioButtonActivity.name = "select-row";
  radioButtonActivity.id = "select-row";

  const radioData = document.createElement("td");
  radioData.classList.add("hidden-radio");
  radioData.appendChild(radioButtonActivity);

  const activityPosData = document.createElement("td");
  activityPosData.classList.add("position");
  activityPosData.id = `position-${Number(indexOfLastActivity) + 1}`;
  activityPosData.textContent = Number(indexOfLastActivity) + 1;

  const activityData = document.createElement("td");
  activityData.classList.add("activity");
  activityData.id = `activity-${Number(indexOfLastActivity) + 1}`;
  activityData.textContent = activityDesc.value;

  const activityStartData = document.createElement("td");
  activityStartData.classList.add("start");
  activityStartData.id = `start-${Number(indexOfLastActivity) + 1}`;
  activityStartData.textContent = activityStart.value;

  const activityFinishData = document.createElement("td");
  activityFinishData.classList.add("finish");
  activityFinishData.id = `finish-${Number(indexOfLastActivity) + 1}`;
  activityFinishData.textContent = activityFinish.value;

  const canvasRow = document.createElement("td");
  canvasRow.classList.add("canvas-rect");

  const canvasData = document.createElement("canvas");
  canvasData.classList.add("chart-rect");
  canvasData.id = `chart-${Number(indexOfLastActivity) + 1}`;
  canvasData.height = "20";
  canvasData.width = "520";

  canvasRow.appendChild(canvasData);

  activityRow.appendChild(radioData);
  activityRow.appendChild(activityPosData);
  activityRow.appendChild(activityData);
  activityRow.appendChild(activityStartData);
  activityRow.appendChild(activityFinishData);
  activityRow.appendChild(canvasRow);

  tableBody.appendChild(activityRow);

  drawChartForNewActivity(Number(indexOfLastActivity) + 1, activityColor.value);

  activityDesc.value = "";
  activityStart.value = "";
  activityFinish.value = "";

  hideAddForm();
}

function drawChartForNewActivity(indexChart, color) {
  const chart = document.querySelector(`#chart-${indexChart}`);
  const ctx = chart.getContext("2d");
  drawActivity(ctx, color);
}

function hideAddForm() {
  hideSubMenu("add-form");
}

/* Functions to delete Activity */
function showDeleteForm() {
  const removeBtn = document.querySelector(".activity-btn-remove");
  removeBtn.addEventListener("click", removeRow);

  showRadioButtons();
  addReturnEvent("remove-form");
  hideMainMenu("remove-form");
}

function removeRow() {
  const activityRows = document.querySelectorAll(".content-row");
  let rowNodeId = selectRowRadioButtons();

  activityRows.forEach((row) => {
    if (row.id.toUpperCase() == rowNodeId.toUpperCase()) {
      const tableBody = document.querySelector(".table-body");
      tableBody.removeChild(row);
    }
  });
}

function hideDeleteForm() {
  hideRadioButtons();
  hideSubMenu("remove-form");
}

/* Function to edit Activity */
function showEditForm() {
  const editBtn = document.querySelector(".edit-btn");
  const editSelectBtn = document.querySelector(".edit-select-btn");
  const editCancelBtn = document.querySelector(".edit-cancel-btn");

  editBtn.addEventListener("click", retriveEditData);
  editSelectBtn.addEventListener("click", replaceValue);
  editCancelBtn.addEventListener("click", cancelEdit);

  showRadioButtons();
  addReturnEvent("edit-form");
  hideMainMenu("edit-form");
}

function retriveEditData() {
  let rowNodeId = selectRowRadioButtons();
  let row = rowNodeId.slice(rowNodeId.indexOf("-") + 1, rowNodeId.length);

  //Old Data
  const activityDescOld = document.querySelector(`#activity-${row}`);
  const activityStartOld = document.querySelector(`#start-${row}`);
  const activityFinishOld = document.querySelector(`#finish-${row}`);
  const activityChartOld = document
    .querySelector(`#chart-${row}`)
    .getContext("2d").fillStyle;

  //Display Form
  const inputEditForm = document.querySelectorAll(".input-edit-form");
  inputEditForm.forEach((singleNode) => singleNode.classList.add("show"));

  //Set Old Data in Form
  hideRadioButtons();
  const activityDesc = document.querySelector("#edit-activity-desc");
  const activityStart = document.querySelector("#edit-activity-start");
  const activityFinish = document.querySelector("#edit-activity-finish");

  activityDesc.value = activityDescOld.textContent;
  activityStart.value = activityStartOld.textContent;
  activityFinish.value = activityFinishOld.textContent;

  switch (activityChartOld) {
    case RED_STRING:
      setOptionalInSelectDefault("red");
      break;

    case BLUE_STRING:
      setOptionalInSelectDefault("blue");
      break;

    case GREEN_STRING:
      setOptionalInSelectDefault("green");
      break;

    case BLACK_STRING:
      setOptionalInSelectDefault("black");
      break;
  }

  const editBtn = document.querySelector(".edit-btn");
  editBtn.style.display = "none";

  const editReturnBtn = document.querySelector(".edit-return-btn");
  editReturnBtn.style.display = "none";

  const editSelectBtn = document.querySelector(".edit-select-btn");
  editSelectBtn.style.display = null;

  const editCancelBtn = document.querySelector(".edit-cancel-btn");
  editCancelBtn.style.display = null;
}

function replaceValue() {
  let rowNodeId = selectRowRadioButtons();
  let row = rowNodeId.slice(rowNodeId.indexOf("-") + 1, rowNodeId.length);

  //Old Data
  const activityDescOld = document.querySelector(`#activity-${row}`);
  const activityStartOld = document.querySelector(`#start-${row}`);
  const activityFinishOld = document.querySelector(`#finish-${row}`);
  const activityChartOld = document.querySelector(`#chart-${row}`);
  const ctx = activityChartOld.getContext("2d");

  const activityDesc = document.querySelector("#edit-activity-desc");
  const activityStart = document.querySelector("#edit-activity-start");
  const activityFinish = document.querySelector("#edit-activity-finish");
  const activityColor = document.querySelector("#edit-activity-color");

  activityDescOld.textContent = activityDesc.value;
  activityStartOld.textContent = activityStart.value;
  activityFinishOld.textContent = activityFinish.value;
  ctx.fillStyle = activityColor.value;
  ctx.fill();

  const inputEditForm = document.querySelectorAll(".input-edit-form");
  inputEditForm.forEach((singleNode) => singleNode.classList.remove("show"));

  const editSelectBtn = document.querySelector(".edit-select-btn");
  editSelectBtn.style.display = "none";

  const editCancelBtn = document.querySelector(".edit-cancel-btn");
  editCancelBtn.style.display = "none";

  const editBtn = document.querySelector(".edit-btn");
  editBtn.style.display = null;

  const editReturnBtn = document.querySelector(".edit-return-btn");
  editReturnBtn.style.display = null;

  showRadioButtons();
}

function cancelEdit() {
  const inputEditForm = document.querySelectorAll(".input-edit-form");
  inputEditForm.forEach((singleNode) => singleNode.classList.remove("show"));

  const editSelectBtn = document.querySelector(".edit-select-btn");
  editSelectBtn.style.display = "none";

  const editCancelBtn = document.querySelector(".edit-cancel-btn");
  editCancelBtn.style.display = "none";

  const editBtn = document.querySelector(".edit-btn");
  editBtn.style.display = null;

  const editReturnBtn = document.querySelector(".edit-return-btn");
  editReturnBtn.style.display = null;

  showRadioButtons();
}

function hideEditForm() {
  hideRadioButtons();
  hideSubMenu("edit-form");
}

// Drag & Drop Functions
function allowDrop(event) {
  event.preventDefault();
}

function dropHandler(event) {
  console.log("Drop");
  event.preventDefault();

  const tableBody = document.querySelector(".table-body");

  const rows = document.querySelectorAll(".content-row");
  let arrayRows = Array.from(rows);

  let rowToDrop = document.querySelector(
    `#${event.dataTransfer.getData("text")}`
  );
  let rowToCopy = document.querySelector(`#${event.target.parentNode.id}`);

  const indexRowToCopy = arrayRows.indexOf(rowToCopy);
  const indexRowToDrop = arrayRows.indexOf(rowToDrop);

  arrayRows[indexRowToCopy] = rowToDrop;
  arrayRows[indexRowToDrop] = rowToCopy;

  while (tableBody.firstChild) {
    tableBody.removeChild(tableBody.firstChild);
  }

  for (let i = 0; i < arrayRows.length; i++) {
    tableBody.appendChild(arrayRows[i]);
  }

  hideDeleteZoneText();
}

function dragHandler(event) {
  console.log("Drag");
  showDeleteZoneText();
  event.dataTransfer.setData("text", event.target.id);
}

//Drag & Drop For Delete Activity
function allowDeleteDrop(event) {
  event.preventDefault();
  event.target.style.display = null;
}

function dropDeleteHandler(event) {
  console.log("Drop Delete");
  event.preventDefault();

  const tableBody = document.querySelector(".table-body");

  let rowToDrop = document.querySelector(
    `#${event.dataTransfer.getData("text")}`
  );

  tableBody.removeChild(rowToDrop);

  hideDeleteZoneText();
}

//Utils
function addReturnEvent(subMenuClass) {
  let typeMenu = subMenuClass.slice(0, subMenuClass.indexOf("-")).toString();

  const returnBtn = document.querySelector(`.${typeMenu}-return-btn`);

  switch (typeMenu) {
    case "add":
      returnBtn.addEventListener("click", hideAddForm);
      break;

    case "remove":
      returnBtn.addEventListener("click", hideDeleteForm);
      break;

    case "edit":
      returnBtn.addEventListener("click", hideEditForm);
      break;
  }
}

function showRadioButtons() {
  const radioButtons = document.querySelectorAll(".hidden-radio");
  radioButtons.forEach((radio) => radio.classList.add("show"));
}

function selectRowRadioButtons() {
  const radioButtons = document.querySelectorAll("#select-row");
  let parentNodeId = "";
  radioButtons.forEach((radio) => {
    if (radio.checked) {
      parentNodeId = radio.parentNode.parentNode.id;
    }
  });
  return parentNodeId;
}

function hideRadioButtons() {
  const radioButtons = document.querySelectorAll(".hidden-radio");
  radioButtons.forEach((radio) => radio.classList.remove("show"));
}

function hideMainMenu(showMenuClass) {
  const menuToShow = document.querySelector(`.${showMenuClass}`);
  const buttons = document.querySelector(".buttons");

  disableDragDropInSubMenu();
  buttons.style.display = "none";
  menuToShow.classList.add("show");
}

function hideSubMenu(subMenuClass) {
  const subMenu = document.querySelector(`.${subMenuClass}`);
  const buttons = document.querySelector(".buttons");

  enableDragDropInMainMenu();
  buttons.style.display = null;
  subMenu.classList.remove("show");
}

function drawActivity(canvasContex, color = "black") {
  let canvasIndexRow = canvasContex.canvas.id.slice(
    canvasContex.canvas.id.indexOf("-") + 1,
    canvasContex.canvas.id.length
  );
  const stratDate = document.querySelector(
    `#start-${canvasIndexRow}`
  ).textContent;
  const finishDate = document.querySelector(
    `#finish-${canvasIndexRow}`
  ).textContent;

  const weeksDifference = convertMStoWeek(
    Date.parse(finishDate) - Date.parse(stratDate)
  );

  const stratDateObj = new Date(stratDate);
  const startPosition = getWeekOfYear(
    stratDateObj.getMonth(),
    stratDateObj.getDate()
  );

  canvasContex.fillStyle = color;
  canvasContex.rect(startPosition * 10, 0, 10 * weeksDifference, 20);
  canvasContex.fill();
}

function convertMStoWeek(dateInMs) {
  return dateInMs / 604800000;
}

function getWeekOfYear(month, day) {
  return 4 * month + day / 7;
}

function maxArrayNumber(array) {
  let max = 0;
  array.forEach((a) => {
    if (a > max) {
      max = a;
    }
  });

  return max;
}

function showDeleteZoneText() {
  const deletePara = document.querySelector(".delete-zone");
  deletePara.style.display = null;
  deletePara.style.border = "1px solid black";
}

function hideDeleteZoneText() {
  const deletePara = document.querySelector(".delete-zone");
  deletePara.style.display = "none";
  deletePara.parentNode.style.border = null;
}

function setOptionalInSelectDefault(color) {
  const activityChart = document.querySelector("#edit-activity-color");

  for (let i = 0; i < activityChart.childElementCount; i++) {
    if (color.toUpperCase() == activityChart.children[i].value.toUpperCase()) {
      activityChart.children[i].selected = "selected";
    }
  }
}

function disableDragDropInSubMenu() {
  const rows = document.querySelectorAll(".content-row");
  rows.forEach((row) => {
    row.draggable = false;
  });
}

function enableDragDropInMainMenu() {
  const rows = document.querySelectorAll(".content-row");
  rows.forEach((row) => {
    row.draggable = true;
  });
}

//Menu Button
const addBtn = document.querySelector(".add-activity");
addBtn.addEventListener("click", showAddForm);

const removeBtn = document.querySelector(".remove-activity");
removeBtn.addEventListener("click", showDeleteForm);

const editBtn = document.querySelector(".edit-activity");
editBtn.addEventListener("click", showEditForm);

//Chart
const charts = document.querySelectorAll(".chart-rect");

drawActivity(charts[0].getContext("2d"), "red");
drawActivity(charts[1].getContext("2d"), "blue");
drawActivity(charts[2].getContext("2d"), "green");

// Drag & Drop
const rows = document.querySelectorAll(".content-row");
rows.forEach((row) => {
  row.addEventListener("drop", dropHandler);
  row.addEventListener("dragover", allowDrop);
  row.addEventListener("dragstart", dragHandler);
});

const deleteDiv = document.querySelector(".delete-zone");
deleteDiv.addEventListener("dragover", allowDeleteDrop);
deleteDiv.addEventListener("drop", dropDeleteHandler);
