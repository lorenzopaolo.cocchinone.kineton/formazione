const allCanvas = document.querySelectorAll(".canvas");


allCanvas.forEach((canvas) => {
    canvas.addEventListener("click", drawCircleWithoutContext);
});


function drawCircleWithoutContext(){
    const localContextCanvas = this.getContext("2d");

    localContextCanvas.fillStyle = "white";
    localContextCanvas.strokeStyle = "white";
    localContextCanvas.beginPath();
    localContextCanvas.arc(25, 25, 20, 0, 2 * Math.PI);
    localContextCanvas.stroke();
    localContextCanvas.fill();
    
    console.log(this.id);
    console.log("drawing circle!");
}




