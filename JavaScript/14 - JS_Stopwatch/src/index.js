const clockTextEl = document.querySelector(".clock-text");

const startBtn = document.querySelector(".start-btn");
const stopBtn = document.querySelector(".stop-btn");
const resetBtn = document.querySelector(".reset-btn");


startBtn.addEventListener("click", startTimer);
stopBtn.addEventListener("click", stopTimer);
resetBtn.addEventListener("click", resetTimer);

let milliseconds = 0;
let seconds = 0;
let minutes = 0;

let intervalID = '';


function startTimer(){
    intervalID = setInterval(() => {

        let stringMillisecond = "";
        let stringSeconds = "";
        let stringMinutes = "";
        
        milliseconds += 1;


        if(milliseconds >= 99){
            milliseconds = 0;
            seconds += 1;
            if(seconds >= 60){
                seconds = 0;
                minutes += 1;
            }
        }

        if(milliseconds <= 9){
            stringMillisecond = '0' + milliseconds;
        } else {
            stringMillisecond = milliseconds;
        }

        if(seconds <= 9){
            stringSeconds = '0' + seconds;
        } else {
            stringSeconds = seconds;
        }

        if(minutes <= 9){
            stringMinutes = '0' + minutes;
        } else {
            stringMinutes = minutes;
        }
       
        clockTextEl.textContent = `${stringMinutes} : ${stringSeconds} : ${stringMillisecond}`;
        
        
    }, 10);
}

function stopTimer(){
    clearInterval(intervalID);
} 

function resetTimer(){
    milliseconds = 0;
    seconds = 0;
    minutes = 0;

    clockTextEl.textContent = `0${minutes} : 0${seconds} : 0${milliseconds}`;

}

