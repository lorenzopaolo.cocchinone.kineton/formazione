const CANVAS_WIDTH = 600;
const CANVAS_HEIGHT = 250;
const TILE = 10;

let snake = [
  { x: 200, y: 200 },
  { x: 210, y: 200 },
  { x: 220, y: 200 },
  { x: 230, y: 200 },
];
let direction = "x";

let fruit = { x: 120, y: 80 };
let eaten = false;

let score = 0;

let gameStatus = false;

const board = document.querySelector(".board-game");
const scoreEl = document.querySelector(".score");

const startBtn = document.querySelector(".start-btn");
const pauseBtn = document.querySelector(".pause-btn");
const resetBtn = document.querySelector(".reset-btn");

startBtn.addEventListener("click", () => (gameStatus = true));
pauseBtn.addEventListener("click", () => (gameStatus = false));
resetBtn.addEventListener("click", () => location.reload());

board.setAttribute("width", CANVAS_WIDTH);
board.setAttribute("height", CANVAS_HEIGHT);

const ctx = board.getContext("2d");

let intervalID = setInterval(() => {
  if (gameStatus) {
    // Drawing Fruit
    if (!eaten) {
      ctx.fillStyle = "red";
      ctx.fillRect(fruit.x, fruit.y, TILE, TILE);
    } else {
      fruit.x = getRandomInt(CANVAS_WIDTH);
      fruit.y = getRandomInt(CANVAS_HEIGHT);
      eaten = false;
    }

    // Drawing Snake
    snake.forEach((part) => {
      ctx.fillStyle = "green";
      ctx.fillRect(part.x, part.y, TILE, TILE);
    });

    // Change Direction
    if (direction == "x") {
      let snakeTail = snake.shift();
      snakePrevHead = structuredClone(snake[snake.length - 1]);
      snakePrevHead.x += TILE;
      snake.push(snakePrevHead);
      ctx.clearRect(snakeTail.x, snakeTail.y, TILE, TILE);
    }

    if (direction == "-x") {
      let snakeTail = snake.shift();
      snakePrevHead = structuredClone(snake[snake.length - 1]);
      snakePrevHead.x -= TILE;
      snake.push(snakePrevHead);
      ctx.clearRect(snakeTail.x, snakeTail.y, TILE, TILE);
    }

    if (direction == "y") {
      let snakeTail = snake.shift();
      snakePrevHead = structuredClone(snake[snake.length - 1]);
      snakePrevHead.y += TILE;
      snake.push(snakePrevHead);
      ctx.clearRect(snakeTail.x, snakeTail.y, TILE, TILE);
    }

    if (direction == "-y") {
      let snakeTail = snake.shift();
      snakePrevHead = structuredClone(snake[snake.length - 1]);
      snakePrevHead.y -= TILE;
      snake.push(snakePrevHead);
      ctx.clearRect(snakeTail.x, snakeTail.y, TILE, TILE);
    }

    // Collisions
    if (
      snake[snake.length - 1].x + TILE == fruit.x + TILE &&
      snake[snake.length - 1].y + TILE == fruit.y + TILE
    ) {
      console.log("Collision detected - Fruit Eaten");
      eaten = true;
      score += 100;
      scoreEl.textContent = `Score: ${score}`;
      let snakePart = structuredClone(snake[0]);
      snakePart.x -= TILE;
      snake.unshift(snakePart);
    }

    if (
      snake[snake.length - 1].x + TILE == 0 ||
      snake[snake.length - 1].y + TILE == 0 ||
      snake[snake.length - 1].x == CANVAS_WIDTH ||
      snake[snake.length - 1].y == CANVAS_HEIGHT
    ) {
      console.log("Collision detected - Game Over");
      scoreEl.textContent = `GAME OVER - Score: ${score}`;
      clearInterval(intervalID);
    }

    let snakeBody = structuredClone(snake);
    snakeBody.pop();
    snakeBody.pop();
    snakeBody.pop();
    snakeBody.pop();

    snakeBody.forEach((part) => {
      if (
        snake[snake.length - 1].x + TILE == part.x + TILE &&
        snake[snake.length - 1].y + TILE == part.y + TILE
      ) {
        console.log("Snake eaten his own body");
        scoreEl.textContent = `GAME OVER - Score: ${score}`;
        console.log(`${snake[snake.length - 1].x}, ${snake[snake.length - 1].y}`);
        console.log(`${snake}`);
        console.log(`${part.x}, ${part.y}`);
        console.log(`${snakeBody}`);
        clearInterval(intervalID);
      }
    });
  }
}, 75);

document.addEventListener("keydown", function (e) {
  e.preventDefault();

  if (e.key == "ArrowLeft") {
    if (direction != "x") direction = "-x";
  }

  if (e.key == "ArrowRight") {
    if (direction != "-x") direction = "x";
  }

  if (e.key == "ArrowUp") {
    if (direction != "y") direction = "-y";
  }

  if (e.key == "ArrowDown") {
    if (direction != "-y") direction = "y";
  }
});

function getRandomInt(max) {
  return Math.floor(Math.floor(Math.random() * max) / 10) * 10;
}
