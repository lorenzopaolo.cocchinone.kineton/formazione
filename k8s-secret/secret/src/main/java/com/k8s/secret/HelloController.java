package com.k8s.secret;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @Value("${custom.one}")
    private String custom_one;

    @GetMapping("/")
    public String index() {
        return "Greetings from Spring Boot! Custom One: " + this.custom_one;
    }
}
